(function() {
    const search = document.querySelector('#search')
    const matchList = document.querySelector('#match-list')

    /**
     * Search state_capitals.json and filter it
     * @param {String} searchText
     */
    const searchStates = async searchText => {
        const res = await fetch('../data/state_capitals.json')

        let matches = []

        if (res.ok && searchText.length > 0) {
            const states = await res.json()

            // get matches to current input
            matches = states.filter(state => {
                const regex = new RegExp(`^${searchText}`, 'gi')
                return state.name.match(regex) || state.abbr.match(regex)
            })
        } else {
            matchList.innerHTML = ''
        }

        outputHtml(matches)
    }

    /**
     * Show results in match-list
     * @param {Array} matches
     */
    const outputHtml = matches => {
        if (matches.length > 0) {
            matchList.innerHTML = matches.map(match => `
                <div class="card card-body mb-1">
                    <h4>${match.name} ${match.abbr} <span class="text-primary">${match.capital}</span></h4>
                    <small>Lat: ${match.lat} / Long: ${match.long}</small>
                </div>
            `).join('')
        }
    }

    search.addEventListener('input', () => searchStates(search.value))
})()
